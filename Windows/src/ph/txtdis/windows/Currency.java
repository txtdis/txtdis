package ph.txtdis.windows;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class Currency extends BigDecimal {

	private static final long serialVersionUID = -2533414977562366884L;

	public Currency(char[] in) {
		super(in);
		// TODO Auto-generated constructor stub
	}

	public Currency(String val) {
		super(val);
		// TODO Auto-generated constructor stub
	}

	public Currency(double val) {
		super(val);
		// TODO Auto-generated constructor stub
	}

	public Currency(BigInteger val) {
		super(val);
		// TODO Auto-generated constructor stub
	}

	public Currency(int val) {
		super(val);
		// TODO Auto-generated constructor stub
	}

	public Currency(long val) {
		super(val);
		// TODO Auto-generated constructor stub
	}

	public Currency(char[] in, MathContext mc) {
		super(in, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(String val, MathContext mc) {
		super(val, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(double val, MathContext mc) {
		super(val, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(BigInteger val, MathContext mc) {
		super(val, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(BigInteger unscaledVal, int scale) {
		super(unscaledVal, scale);
		// TODO Auto-generated constructor stub
	}

	public Currency(int val, MathContext mc) {
		super(val, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(long val, MathContext mc) {
		super(val, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(char[] in, int offset, int len) {
		super(in, offset, len);
		// TODO Auto-generated constructor stub
	}

	public Currency(BigInteger unscaledVal, int scale, MathContext mc) {
		super(unscaledVal, scale, mc);
		// TODO Auto-generated constructor stub
	}

	public Currency(char[] in, int offset, int len, MathContext mc) {
		super(in, offset, len, mc);
		// TODO Auto-generated constructor stub
	}

}
